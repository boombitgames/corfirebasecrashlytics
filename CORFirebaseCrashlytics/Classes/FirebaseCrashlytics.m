#import "FirebaseCrashlytics.h"
#import <FirebaseCrashlytics/FirebaseCrashlytics.h>

// Converts C style string to NSString
#define GetStringParam( _x_ ) ( _x_ != NULL ) ? [NSString stringWithUTF8String:_x_] : [NSString stringWithUTF8String:""]

@implementation FirebaseCrashlytics

void CrashlyticsSetCustomKeyString(char* key, char* value)
{
    [[FIRCrashlytics crashlytics] setCustomValue:GetStringParam(value) forKey:GetStringParam(key)];
}

void CrashlyticsSetCustomKeyInt(char* key, int value)
{
    [[FIRCrashlytics crashlytics] setCustomValue:@(value) forKey:GetStringParam(key)];
}

void CrashlyticsSetCustomKeyFloat(char* key, float value)
{
    [[FIRCrashlytics crashlytics] setCustomValue:@(value) forKey:GetStringParam(key)];
}

void CrashlyticsSetCustomKeyBool(char* key, bool value)
{
    [[FIRCrashlytics crashlytics] setCustomValue:@(value) forKey:GetStringParam(key)];
}

void CrashlyticsLogMessage(char* message)
{
    [[FIRCrashlytics crashlytics] log:GetStringParam(message)];
}

void CrashlyticsSetUserId(char* userId)
{
    [[FIRCrashlytics crashlytics] setUserID:GetStringParam(userId)];
}

void CrashlyticsLogException(NSError* error)
{
    [[FIRCrashlytics crashlytics] recordError:error];
}

void CrashlyticsSetCollectionEnabled(bool enabled)
{
    [[FIRCrashlytics crashlytics] setCrashlyticsCollectionEnabled:enabled];
}

@end
