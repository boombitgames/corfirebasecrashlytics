//
//  main.m
//  CORFirebaseCrashlytics
//
//  Created by Damian Grzybinski on 12/02/2021.
//  Copyright (c) 2021 Damian Grzybinski. All rights reserved.
//

@import UIKit;
#import "CORFIREBASECRASHLYTICSAppDelegate.h"

int main(int argc, char * argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([CORFIREBASECRASHLYTICSAppDelegate class]));
    }
}
